package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findAllByLastNameIgnoreCaseContaining(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office IS NOT NULL AND u.office.city = :city")
    List<User> findAllByCity(String city, Sort sort);

    List<User> findAllByExperienceGreaterThanEqual(int experience, Sort sort);

    @Query("SELECT u FROM User u " +
            "WHERE u.office IS NOT NULL AND u.office.city = :city " +
            "AND u.team IS NOT NULL AND u.team.room = :room")
    List<User> findAllByCityAndRoom(String city, String room, Sort sort);

    @Transactional
    int deleteByExperienceLessThan(int experience);
}
