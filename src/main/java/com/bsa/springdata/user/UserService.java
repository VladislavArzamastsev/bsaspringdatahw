package com.bsa.springdata.user;

import com.bsa.springdata.office.OfficeRepository;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final OfficeRepository officeRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public UserService(UserRepository userRepository, OfficeRepository officeRepository, TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.officeRepository = officeRepository;
        this.teamRepository = teamRepository;
    }

    public Optional<UUID> safeCreateUser(CreateUserDto userDto) {
        var office = officeRepository.findById(userDto.getOfficeId());
        var team = teamRepository.findById((userDto.getTeamId()));

        return office.flatMap(o -> team.map(t -> {
            var user = User.fromDto(userDto, o, t);
            var result = userRepository.save(user);
            return result.getId();
        }));
    }

    public Optional<UUID> createUser(CreateUserDto userDto) {
        try {
            var office = officeRepository.getOne(userDto.getOfficeId());
            var team = teamRepository.getOne(userDto.getTeamId());

            var user = User.fromDto(userDto, office, team);
            var result = userRepository.save(user);
            return Optional.of(result.getId());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<UserDto> getUserById(UUID id) {
        return userRepository.findById(id).map(UserDto::fromEntity);
    }

    public List<UserDto> getUsers() {
        return mapToDtoList(userRepository.findAll());
    }

    public List<UserDto> findByLastName(String lastName, int page, int size) {
        Sort sort = Sort.by("lastName");
        Pageable pageable = PageRequest.of(page, size, sort);
        List<User> users = userRepository.findAllByLastNameIgnoreCaseContaining(lastName, pageable);
        return mapToDtoList(users);
    }

    public List<UserDto> findByCity(String city) {
        Sort sort = Sort.by("lastName").ascending();
        return mapToDtoList(userRepository.findAllByCity(city, sort));
    }

    public List<UserDto> findByExperience(int experience) {
        Sort sort = Sort.by("experience").descending();
        return mapToDtoList(userRepository.findAllByExperienceGreaterThanEqual(experience, sort));
    }

    public List<UserDto> findByRoomAndCity(String city, String room) {
        Sort sort = Sort.by("lastName");
        return mapToDtoList(userRepository.findAllByCityAndRoom(city, room, sort));
    }

    public int deleteByExperience(int experience) {
        return userRepository.deleteByExperienceLessThan(experience);
    }

    private List<UserDto> mapToDtoList(List<? extends User> users){
        return users.
                stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }
}
