package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    private final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        //  You can use several queries here. Try to keep it as simple as possible
//        описать логику обновления технологии у команды, в которой участников < определенного количества.
//        Можно использовать несколько запросов
        teamRepository.updateTechnology(devsNumber, oldTechnologyName, newTechnologyName);
    }

    public void normalizeName(String name) {
//        описать один запрос, который будет добавлять к имени указанной команды название проекта
//        и название технологии (Team_Project_Technology). Необходимо использовать nativeQuery
        // Use a single query. You need to create a native query
        teamRepository.normalizeName(name);
    }
}
