package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findByName(String teamName);

    long countByTechnologyName(String newTechnology);

    @Transactional
    @Query(
            value = "UPDATE Team t " +
                    "SET t.technology = (SELECT tech FROM Technology tech WHERE tech.name = :newTechnologyName) " +
                    "    WHERE t.users.size < :devsNumber " +
                    "        AND t.technology = (SELECT tech FROM Technology tech WHERE tech.name = :oldTechnologyName)"
    )
    @Modifying
    void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);

    @Transactional
    @Modifying
    @Query(value =
            "UPDATE teams " +
                    "SET name = CONCAT(" +
                    "    name," +
                    "    '_'," +
                    "    (SELECT projects.name FROM projects WHERE projects.id = project_id)," +
                    "    '_'," +
                    "    (SELECT technologies.name FROM technologies WHERE technologies.id = technology_id)) " +
                    "WHERE name = :name",
            nativeQuery = true)
    void normalizeName(String name);
}
