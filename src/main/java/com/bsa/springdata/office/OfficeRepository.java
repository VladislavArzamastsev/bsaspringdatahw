package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT o " +
            "FROM Office o INNER JOIN User u ON u.office.id = o.id " +
            "INNER JOIN Team t ON t.id = u.team.id " +
            "INNER JOIN Technology tech ON tech.id = t.technology.id AND tech.name = :name")
    List<Office> getByTechnology(String name);

    @Transactional
    @Modifying
    @Query(value =
            "UPDATE Office o " +
                    "SET o.address = :newAddress " +
                    "WHERE o.address = :oldAddress AND o.users.size <> 0")
    void updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);
}
