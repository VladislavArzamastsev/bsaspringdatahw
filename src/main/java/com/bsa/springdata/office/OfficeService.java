package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfficeService {

    private final OfficeRepository officeRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public List<OfficeDto> getByTechnology(String technology) {
//        описать поиск офисов, в которых есть проекты, которые используют определенную технологию.
        //  Use single query to get data.
        return officeRepository.getByTechnology(technology)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
//        Обновление существующего офиса
//        описать логику обновления адреса у офиса, если разработчики этого офиса участвуют в проекте
        //  Use single method to update address. In order to get the new office you can make extra query
        //  Hint: Every user is connected to one of the project. There cannot be any users without a project.
        officeRepository.updateAddress(oldAddress, newAddress);
        return officeRepository.findByAddress(newAddress)
                .map(OfficeDto::fromEntity);
    }
}
