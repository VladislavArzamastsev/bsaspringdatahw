package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(
            value = "SELECT COUNT(DISTINCT projects.id) " +
                    "FROM projects " +
                    "INNER JOIN teams t on projects.id = t.project_id " +
                    "INNER JOIN users u on t.id = u.team_id AND u.id IN " +
                    "           (SELECT id FROM users INNER JOIN user2role u2r " +
                    "                                  ON u2r.user_id = users.id AND u2r.role_id = (SELECT roles.id " +
                    "                                                    FROM roles " +
                    "                                                    WHERE name = :name)) ",
            nativeQuery = true)
    int getCountWithRoleName(String name);

    @Query("SELECT p " +
            "FROM Project p INNER JOIN Team team ON p.id = team.project.id " +
            "   INNER JOIN Technology tech ON tech.id = team.technology.id AND tech.name = :technologyName " +
            "   INNER JOIN User u ON team.id = u.team.id " +
            "GROUP BY p " +
            "ORDER BY COUNT(DISTINCT u.id)")
    List<Project> findTopProjectsByTechnology(String technologyName, Pageable pageable);

    @Query("SELECT p " +
            "FROM Project p INNER JOIN Team t ON p.id = t.project.id " +
            "INNER JOIN User u ON t.id = u.team.id " +
            "GROUP BY p " +
            "ORDER BY COUNT(DISTINCT t.id) DESC, COUNT(DISTINCT u.id) DESC, p.name DESC ")
    List<Project> findBiggestProjects(Pageable pageable);

    @Query(value =
            "SELECT projects.name AS name, COUNT(DISTINCT t.id) AS teamsNumber, COUNT(DISTINCT u.id) AS developersNumber, " +
                    "string_agg(DISTINCT tech.name, ',' ORDER BY tech.name DESC) AS technologies " +
                    "FROM projects INNER JOIN teams t ON projects.id = t.project_id " +
                    "    INNER JOIN users u ON t.id = u.team_id " +
                    "    INNER JOIN technologies tech ON tech.id = t.technology_id " +
                    "GROUP BY projects.name " +
                    "ORDER BY projects.name",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}