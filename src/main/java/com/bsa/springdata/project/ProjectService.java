package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final TechnologyRepository technologyRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository,
                          TechnologyRepository technologyRepository,
                          TeamRepository teamRepository) {
        this.projectRepository = projectRepository;
        this.technologyRepository = technologyRepository;
        this.teamRepository = teamRepository;
    }

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        //  Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        Pageable pageable = PageRequest.of(0, 5);
        return projectRepository.findTopProjectsByTechnology(technology, pageable)
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        //  Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        Pageable pageable = PageRequest.of(0, 1);
        List<Project> biggestProjects = projectRepository.findBiggestProjects(pageable);
        if (biggestProjects.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(biggestProjects.get(0))
                .map(ProjectDto::fromEntity);
    }

    public List<ProjectSummaryDto> getSummary() {
        // Try to use native query and projection first. If it fails try to make as few queries as possible
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        // Use a single query
//        описать подсчет проектов, в которых есть разработчики, которые занимают определенную роль
        return projectRepository.getCountWithRoleName(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        // Use common JPARepository methods. Build entities in memory and then persist them
        Project project = new Project(
                null,
                createProjectRequest.getProjectName(),
                createProjectRequest.getProjectDescription(),
                null);

        Optional<Team> teamOptional = teamRepository.findByName(createProjectRequest.getTeamName());
        if (teamOptional.isEmpty()) {
            Technology technology = technologyRepository.findByName(createProjectRequest.getTech())
                    .orElse(new Technology(
                            null,
                            createProjectRequest.getTech(),
                            createProjectRequest.getTechDescription(),
                            createProjectRequest.getTechLink()));
            teamOptional = Optional.of(new Team(
                    null,
                    createProjectRequest.getTeamName(),
                    createProjectRequest.getTeamRoom(),
                    createProjectRequest.getTeamArea(),
                    null,
                    new ArrayList<>(),
                    technology));
        }
        Team team = teamOptional.get();
        team.setProject(project);
        List<Team> teams = new ArrayList<>();
        teams.add(team);
        project.setTeams(teams);
        return projectRepository.save(project).getId();
    }
}
